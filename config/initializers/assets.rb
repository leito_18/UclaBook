# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( rapid-icons.css )
Rails.application.config.assets.precompile += %w( main.js )
Rails.application.config.assets.precompile += %w( jquery-2.1.3.min.js )

Rails.application.config.assets.precompile += %w( jquery.easing.min.js )
Rails.application.config.assets.precompile += %w( jquery.cycle.all.min.js )
Rails.application.config.assets.precompile += %w( jquery.form.js )
Rails.application.config.assets.precompile += %w( jquery.maximage.min.js )
Rails.application.config.assets.precompile += %w( classie.js )
Rails.application.config.assets.precompile += %w( pathLoader.js )
Rails.application.config.assets.precompile += %w( preloader.js )

Rails.application.config.assets.precompile += %w( count_down.js )
Rails.application.config.assets.precompile += %w( happy.js )
Rails.application.config.assets.precompile += %w( happy.methods.js )
Rails.application.config.assets.precompile += %w( retina.js )
Rails.application.config.assets.precompile += %w( waypoints.min.js )
Rails.application.config.assets.precompile += %w( nivo-lightbox.min.js )
Rails.application.config.assets.precompile += %w( jquery.fitvids.js )

Rails.application.config.assets.precompile += %w( jquery.stellar.js )
Rails.application.config.assets.precompile += %w( owl.carousel.js )











