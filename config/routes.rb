Rails.application.routes.draw do
  
  
  resources :links
  resources :messages do
    member do
      put "like" => "messages#upvote"
      put "unlike" => "messages#downvote"
    end
  end
  mount Commontator::Engine => '/commontator'
  resources :followers
  resources :followeds
  resources :canals
  resources :preferencia

  resources :usuarios, as: :users, only: [:show,:update]  #esto es una prueba, que si funciono 

  resources :relationships, only: [:create,:update,:index]

 
  resources :editlandings
  resources :pais
  get '/index', to: 'landing#index'
  get '/informacion', to: 'landing#informacion'
  get '/perfil', to: 'dashboard#profile'


  #administrar landing 
  get '/landingpage', to: 'editlandings#index'
  #administrar landing



  root 'dashboard#index'
   
  devise_for :users, controllers: { sessions: "users/sessions", :registrations => "users/registrations", :confirmations => "users/confirmations" }
   match '/users/:id/canals/:id', controller: 'canals', action: 'update', via: 'post'
          
  resources :users do
     resources :favoritos
  	 resources :canals do

       

     end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
