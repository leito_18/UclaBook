json.extract! favorito, :id, :preferencia_id, :created_at, :updated_at
json.url favorito_url(favorito, format: :json)