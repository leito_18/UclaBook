json.extract! preferencium, :id, :nombre, :created_at, :updated_at
json.url preferencium_url(preferencium, format: :json)