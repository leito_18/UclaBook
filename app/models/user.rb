 require "httparty"
 require "json"
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
 
  include HTTParty
 acts_as_commontator
 acts_as_voter

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  dragonfly_accessor :fotoperfil
  dragonfly_accessor :portada

  has_many :canals
  has_many :messages
  has_many :favoritos
 
  has_many :relationships
#  has_many :active_relationships, class_name:  "relationship",
#                                  foreign_key: "follower_id",
 #                                 dependent:   :destroy

 # has_many :passive_relationships, class_name:  "relationship",
  #                                 foreign_key: "followed_id",
   #                                dependent:   :destroy
  
  #has_many :following, through: :active_relationships, source: :followed

  #has_many :followers, through: :passive_relationships, source: :follower
  def follow?(followed)
    Relationship.relations?(self,followed)
  end


    # Follows a user.
  #def follow(other_user)
   # following << other_user
  #end

  # Unfollows a user.
  #def unfollow(other_user)
   # following.delete(other_user)
  #end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end
    def self.abc

  	@json = get("http://localhost:4040/graduateds.json")
  	@json = @json.to_json
  	@parsed_json = ActiveSupport::JSON.decode(@json)
  	@no = @parsed_json

  	return @no

  end

end
