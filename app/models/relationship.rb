class Relationship < ApplicationRecord
	include AASM

  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"

  validates :follower_id, uniqueness: {scope: :followed_id}

  #validates :follower, presence: true
  #validates :followed, presence: true

  def self.relations?(follower,followed)
  	Relationship.where(follower:follower,followed:followed)
  				.or(Relationship.where(follower:followed,followed:follower))
  				.any?
  end

  def self.pending_for_user(follower)
  	Relationship.pending.where(followed: follower)
  end

	aasm column: "status" do 
	 	state :pending
	 	state :active, initial: true
	 	state :denied

	 	event :accepted do
	 		transitions from: [:pending], to: [:active]
	 	end

	 	event :rejected do
	 		transitions from: [:pending,:active], to: [:denied]
	 	end
	 end
end
