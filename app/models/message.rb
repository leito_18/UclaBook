class Message < ApplicationRecord
	belongs_to :user
	belongs_to :canal
	dragonfly_accessor :archivo
	acts_as_votable
	acts_as_commontable
end
