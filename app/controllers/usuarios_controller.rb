class UsuariosController < ApplicationController
	before_action :set_user


	def show

    @are_relations = current_user.follow?(@user)

	end
	
	 #actualizar el usuario
  def update
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to '/users/'+current_user.id.to_s+'/edit', notice: 'El usuario '+@user.email+' fué actualizado satisfactoriamente' }
        #format.json { render :show, status: :ok, location: @user }
      else
        format.html { redirect_to '/users/'+current_user.id.to_s+'/edit' }
        #format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


	private
		def set_user
			@user = User.find(params[:id])
		end

		def user_params
      		params.require(:user).permit(:facebook, :twitter, :instagram, :linkedin, :website,:documentoidentidad,:portada,:pais, :estado, :ciudad, :codigopostal, :fechanacimiento, :fotoperfil, :password, :password_confirmation, :nombre, :apellido)
    	end
end
