class ApplicationController < ActionController::Base
  #before_action :authenticate_user!
  protect_from_forgery with: :null_session

  #before_filter :authenticate_user!, except: [:index, :informacion]


  before_action :configure_permitted_parameters, if: :devise_controller?
  
  #protect_from_forgery with: :exception
  protected 

  def configure_permitted_parameters
  devise_parameter_sanitizer.permit(:sign_up) do |user_params|
    user_params.permit(:documentoidentidad, :email, :password, :password_confirmation, :remember_me)
  end
end

end
