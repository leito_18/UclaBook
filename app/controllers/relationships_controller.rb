class RelationshipsController < ApplicationController

	before_action :find_followed,except: [:index]

	def index
		@pending_relationships = Relationship.pending_for_user(current_user)
    #@relationships = Relationship.all
    @solicitud = Relationship.all
  	end

  	def show
  	 
  	end

  	# def new
    #@relationships = Relationship.new
  	#end

	def create
		relationship = Relationship.new(follower: current_user, followed: @followed)
		respond_to do |format|
			if relationship.save
				format.html {redirect_to @followed}
				format.js
			else
				format.html {redirect_to @followed, notice:"Error con la solicitud de seguimiento"}
				format.js
			end
		end


	end 

	private
	def find_followed
		@followed = User.find(params[:followed_id])	
	end 


	def find_model
		@model = Relationships.find(params[:id]) if params [:id]
	 end 
end 