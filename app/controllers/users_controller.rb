class UsersController < ApplicationController
 before_action :configure_sign_up_params, only: [:create]
 before_action :configure_account_update_params, only: [:update]

 #editar usuario /users/:id/edit
 def edit
  @user = User.find(params[:id])
 end

 #ver usuario /users/:id/
 def show
  @idbus = params[:id]
  @messages = Message.where(user_id: @idbus)
 end
 
 #actualizar el usuario
  def update
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to '/users/'+current_user.id.to_s+'/edit', notice: 'El usuario '+@user.email+' fué actualizado satisfactoriamente' }
        #format.json { render :show, status: :ok, location: @user }
      else
        format.html { redirect_to '/users/'+current_user.id.to_s+'/edit' }
        #format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end




 protected

  # If you have extra params to permit, append them to the sanitizer.
   def configure_sign_up_params
      params.require(:user).permit(:sign_up, keys: [:facebook, :twitter, :instagram, :linkedin, :website, :documentoidentidad, :portada, :pais, :estado, :ciudad, :codigopostal, :fechanacimiento,:fotoperfil, :password, :password_confirmation, :nombre, :apellido])
   end

  # If you have extra params to permit, append them to the sanitizer.
   def configure_account_update_params
      params.require(:user).permit(:account_update, keys: [:facebook, :twitter, :instagram, :linkedin, :website,:documentoidentidad,:portada, :pais, :estado, :ciudad, :codigopostal, :fechanacimiento,:fotoperfil, :password, :password_confirmation, :nombre, :apellido])
   end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user= User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:facebook, :twitter, :instagram, :linkedin, :website,:documentoidentidad,:portada,:pais, :estado, :ciudad, :codigopostal, :fechanacimiento, :fotoperfil, :password, :password_confirmation, :nombre, :apellido)
    end
  
end
