class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
  skip_before_action :verify_authenticity_token
  # GET /messages
  # GET /messages.json

  def upvote
    @message.upvote_from current_user
    redirect_to messages_path
  end

  def downvote
    @message.downvote_from current_user
    redirect_to messages_path
  end   

  #Este if hace que muestre los posts de el usuario actual
  def index
    
    if current_user
    @messages = current_user.messages.all
    else
      redirect_to "/users/sign_in"
    end

    @nombreusuario = current_user.nombre
    
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
    @canals = Canal.all
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = current_user.messages.new(message_params)

    respond_to do |format|
      if @message.save
        format.html { redirect_to '/users/'+current_user.id.to_s+"/canals/"+params[:canal].to_s, notice: 'Message was successfully created.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:tipo, :archivo, :descripcion, :canal_id, :user_id)
    end
end
