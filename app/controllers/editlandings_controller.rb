class EditlandingsController < ApplicationController
  before_action :set_editlanding, only: [:show, :edit, :update, :destroy]

  # GET /editlandings
  # GET /editlandings.json
  def index
    @editlandings = Editlanding.all
  end

  # GET /editlandings/1
  # GET /editlandings/1.json
  def show
  end

  # GET /editlandings/new
  def new
    @editlanding = Editlanding.new
    

  end

  # GET /editlandings/1/edit
  def edit
    @landing = Editlanding.find(1)
  end

  # POST /editlandings
  # POST /editlandings.json
  def create
    @editlanding = Editlanding.new(editlanding_params)

    respond_to do |format|
      if @editlanding.save
        format.html { redirect_to @editlanding, notice: 'Editlanding was successfully created.' }
        format.json { render :show, status: :created, location: @editlanding }
      else
        format.html { render :new }
        format.json { render json: @editlanding.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /editlandings/1
  # PATCH/PUT /editlandings/1.json
  def update
    respond_to do |format|
      if @editlanding.update(editlanding_params)
        format.html { redirect_to @editlanding, notice: 'Editlanding was successfully updated.' }
        format.json { render :show, status: :ok, location: @editlanding }
      else
        format.html { render :edit }
        format.json { render json: @editlanding.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /editlandings/1
  # DELETE /editlandings/1.json
  def destroy
    @editlanding.destroy
    respond_to do |format|
      format.html { redirect_to editlandings_url, notice: 'Editlanding was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_editlanding
      @editlanding = Editlanding.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def editlanding_params
      params.require(:editlanding).permit(:banner,:texto1, :textobanner, :titulo_seccion2, :texto1_seccion2, :texto2_seccion2, :imagenseccion2, :bannernewsletter, :titulonewsletter, :textonewsletter, :portafolio_titulo, :portafolio_texto, :contactotituloizq, :contactotextoizq, :contactodireccion, :contactotelefono, :contactocorreo, :contactonombre, :footerfacebook, :footertwitter, :footerinstagram, :footerlinkedin, :footergoogle)
    end
end
