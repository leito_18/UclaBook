class FollowedsController < ApplicationController
  before_action :set_followed, only: [:show, :edit, :update, :destroy]

  # GET /followeds
  # GET /followeds.json
  def index
    @followeds = Followed.all
  end

  # GET /followeds/1
  # GET /followeds/1.json
  def show
  end

  # GET /followeds/new
  def new
    @followed = Followed.new
  end

  # GET /followeds/1/edit
  def edit
  end

  # POST /followeds
  # POST /followeds.json
  def create
    @followed = Followed.new(followed_params)

    respond_to do |format|
      if @followed.save
        format.html { redirect_to @followed, notice: 'Followed was successfully created.' }
        format.json { render :show, status: :created, location: @followed }
      else
        format.html { render :new }
        format.json { render json: @followed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /followeds/1
  # PATCH/PUT /followeds/1.json
  def update
    respond_to do |format|
      if @followed.update(followed_params)
        format.html { redirect_to @followed, notice: 'Followed was successfully updated.' }
        format.json { render :show, status: :ok, location: @followed }
      else
        format.html { render :edit }
        format.json { render json: @followed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /followeds/1
  # DELETE /followeds/1.json
  def destroy
    @followed.destroy
    respond_to do |format|
      format.html { redirect_to followeds_url, notice: 'Followed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_followed
      @followed = Followed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def followed_params
      params.require(:followed).permit(:contar)
    end
end
