class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.integer :tipo
      t.string :archivo
      t.string :descripcion

      t.timestamps
    end
  end
end
