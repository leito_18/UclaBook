class AddPreferenciaIdToCanals < ActiveRecord::Migration[5.0]
  def change
    add_column :canals, :preferencia_id, :integer
  end
end
