class CreateFavoritos < ActiveRecord::Migration[5.0]
  def change
    create_table :favoritos do |t|
      t.integer :preferencia_id

      t.timestamps
    end
  end
end
