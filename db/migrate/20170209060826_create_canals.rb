class CreateCanals < ActiveRecord::Migration[5.0]
  def change
    create_table :canals do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
