class CreateEditlandings < ActiveRecord::Migration[5.0]
  def change
    create_table :editlandings do |t|
      t.string :banner_name
      t.string :banner_uid
      t.string :textobanner
      t.string :titulo_seccion2
      t.string :texto1_seccion2
      t.string :texto2_seccion2
      t.string :imagenseccion2_name
      t.string :imagenseccion2_uid
      t.string :bannernewsletter_name
      t.string :bannernewsletter_uid
      t.string :titulonewsletter
      t.string :textonewsletter
      t.string :portafolio_titulo
      t.string :portafolio_texto
      t.string :contactotituloizq
      t.string :contactotextoizq
      t.string :contactodireccion
      t.string :contactotelefono
      t.string :contactocorreo
      t.string :contactonombre
      t.string :footerfacebook
      t.string :footertwitter
      t.string :footerinstagram
      t.string :footerlinkedin
      t.string :footergoogle

      t.timestamps
    end
  end
end
