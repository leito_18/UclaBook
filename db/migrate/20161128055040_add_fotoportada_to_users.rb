class AddFotoportadaToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :portada_uid, :string
    add_column :users, :portada_name, :string
  end
end
