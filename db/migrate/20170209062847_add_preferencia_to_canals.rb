class AddPreferenciaToCanals < ActiveRecord::Migration[5.0]
  def change
    add_column :canals, :preferencia, :string
  end
end
