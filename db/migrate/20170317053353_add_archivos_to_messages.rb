class AddArchivosToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :archivo_name, :string
    add_column :messages, :archivo_uid, :string
  end
end
