class AddUserIdToCanals < ActiveRecord::Migration[5.0]
  def change
    add_reference :canals, :user, index: true
    add_foreign_key :canals, :users
  end
end
