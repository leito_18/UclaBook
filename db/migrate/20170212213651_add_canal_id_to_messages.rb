class AddCanalIdToMessages < ActiveRecord::Migration[5.0]
  def change
    add_reference :messages, :canal, index: true
    add_foreign_key :messages, :canals
  end
end
