class AddPaisCiudadEstadoCpToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :pais, :string
    add_column :users, :ciudad, :string
    add_column :users, :estado, :string
    add_column :users, :codigopostal, :string
  end
end
