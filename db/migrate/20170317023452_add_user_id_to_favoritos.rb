class AddUserIdToFavoritos < ActiveRecord::Migration[5.0]
  def change
    add_reference :favoritos, :user, index: true
    add_foreign_key :favoritos, :users
  end
end
