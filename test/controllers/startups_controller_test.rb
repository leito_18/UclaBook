require 'test_helper'

class StartupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startup = startups(:one)
  end

  test "should get index" do
    get startups_url
    assert_response :success
  end

  test "should get new" do
    get new_startup_url
    assert_response :success
  end

  test "should create startup" do
    assert_difference('Startup.count') do
      post startups_url, params: { startup: { descripcion: @startup.descripcion, facebook: @startup.facebook, google: @startup.google, instagram: @startup.instagram, linkedin: @startup.linkedin, logo_name: @startup.logo_name, logo_origen_name: @startup.logo_origen_name, logo_origen_uid: @startup.logo_origen_uid, logo_uid: @startup.logo_uid, moneda: @startup.moneda, nombre: @startup.nombre, nombre_origen: @startup.nombre_origen, pais_incorporacion: @startup.pais_incorporacion, portada_name: @startup.portada_name, portada_uid: @startup.portada_uid, problema_descripcion: @startup.problema_descripcion, solucion_descripcion: @startup.solucion_descripcion, tipo_origen: @startup.tipo_origen, twitter: @startup.twitter, valoracion: @startup.valoracion, video_name: @startup.video_name, video_uid: @startup.video_uid, website: @startup.website } }
    end

    assert_redirected_to startup_url(Startup.last)
  end

  test "should show startup" do
    get startup_url(@startup)
    assert_response :success
  end

  test "should get edit" do
    get edit_startup_url(@startup)
    assert_response :success
  end

  test "should update startup" do
    patch startup_url(@startup), params: { startup: { descripcion: @startup.descripcion, facebook: @startup.facebook, google: @startup.google, instagram: @startup.instagram, linkedin: @startup.linkedin, logo_name: @startup.logo_name, logo_origen_name: @startup.logo_origen_name, logo_origen_uid: @startup.logo_origen_uid, logo_uid: @startup.logo_uid, moneda: @startup.moneda, nombre: @startup.nombre, nombre_origen: @startup.nombre_origen, pais_incorporacion: @startup.pais_incorporacion, portada_name: @startup.portada_name, portada_uid: @startup.portada_uid, problema_descripcion: @startup.problema_descripcion, solucion_descripcion: @startup.solucion_descripcion, tipo_origen: @startup.tipo_origen, twitter: @startup.twitter, valoracion: @startup.valoracion, video_name: @startup.video_name, video_uid: @startup.video_uid, website: @startup.website } }
    assert_redirected_to startup_url(@startup)
  end

  test "should destroy startup" do
    assert_difference('Startup.count', -1) do
      delete startup_url(@startup)
    end

    assert_redirected_to startups_url
  end
end
