require 'test_helper'

class TrackrecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trackrecord = trackrecords(:one)
  end

  test "should get index" do
    get trackrecords_url
    assert_response :success
  end

  test "should get new" do
    get new_trackrecord_url
    assert_response :success
  end

  test "should create trackrecord" do
    assert_difference('Trackrecord.count') do
      post trackrecords_url, params: { trackrecord: { descripcion: @trackrecord.descripcion, imagen1_name: @trackrecord.imagen1_name, imagen1_uid: @trackrecord.imagen1_uid, imagen2-uid: @trackrecord.imagen2-uid, imagen2_name: @trackrecord.imagen2_name, link: @trackrecord.link, nombre: @trackrecord.nombre, tipo: @trackrecord.tipo } }
    end

    assert_redirected_to trackrecord_url(Trackrecord.last)
  end

  test "should show trackrecord" do
    get trackrecord_url(@trackrecord)
    assert_response :success
  end

  test "should get edit" do
    get edit_trackrecord_url(@trackrecord)
    assert_response :success
  end

  test "should update trackrecord" do
    patch trackrecord_url(@trackrecord), params: { trackrecord: { descripcion: @trackrecord.descripcion, imagen1_name: @trackrecord.imagen1_name, imagen1_uid: @trackrecord.imagen1_uid, imagen2-uid: @trackrecord.imagen2-uid, imagen2_name: @trackrecord.imagen2_name, link: @trackrecord.link, nombre: @trackrecord.nombre, tipo: @trackrecord.tipo } }
    assert_redirected_to trackrecord_url(@trackrecord)
  end

  test "should destroy trackrecord" do
    assert_difference('Trackrecord.count', -1) do
      delete trackrecord_url(@trackrecord)
    end

    assert_redirected_to trackrecords_url
  end
end
