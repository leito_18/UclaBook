require 'test_helper'

class StartupsectorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startupsector = startupsectors(:one)
  end

  test "should get index" do
    get startupsectors_url
    assert_response :success
  end

  test "should get new" do
    get new_startupsector_url
    assert_response :success
  end

  test "should create startupsector" do
    assert_difference('Startupsector.count') do
      post startupsectors_url, params: { startupsector: { nombre: @startupsector.nombre } }
    end

    assert_redirected_to startupsector_url(Startupsector.last)
  end

  test "should show startupsector" do
    get startupsector_url(@startupsector)
    assert_response :success
  end

  test "should get edit" do
    get edit_startupsector_url(@startupsector)
    assert_response :success
  end

  test "should update startupsector" do
    patch startupsector_url(@startupsector), params: { startupsector: { nombre: @startupsector.nombre } }
    assert_redirected_to startupsector_url(@startupsector)
  end

  test "should destroy startupsector" do
    assert_difference('Startupsector.count', -1) do
      delete startupsector_url(@startupsector)
    end

    assert_redirected_to startupsectors_url
  end
end
