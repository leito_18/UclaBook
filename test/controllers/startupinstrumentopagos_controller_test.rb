require 'test_helper'

class StartupinstrumentopagosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startupinstrumentopago = startupinstrumentopagos(:one)
  end

  test "should get index" do
    get startupinstrumentopagos_url
    assert_response :success
  end

  test "should get new" do
    get new_startupinstrumentopago_url
    assert_response :success
  end

  test "should create startupinstrumentopago" do
    assert_difference('Startupinstrumentopago.count') do
      post startupinstrumentopagos_url, params: { startupinstrumentopago: { nombre: @startupinstrumentopago.nombre } }
    end

    assert_redirected_to startupinstrumentopago_url(Startupinstrumentopago.last)
  end

  test "should show startupinstrumentopago" do
    get startupinstrumentopago_url(@startupinstrumentopago)
    assert_response :success
  end

  test "should get edit" do
    get edit_startupinstrumentopago_url(@startupinstrumentopago)
    assert_response :success
  end

  test "should update startupinstrumentopago" do
    patch startupinstrumentopago_url(@startupinstrumentopago), params: { startupinstrumentopago: { nombre: @startupinstrumentopago.nombre } }
    assert_redirected_to startupinstrumentopago_url(@startupinstrumentopago)
  end

  test "should destroy startupinstrumentopago" do
    assert_difference('Startupinstrumentopago.count', -1) do
      delete startupinstrumentopago_url(@startupinstrumentopago)
    end

    assert_redirected_to startupinstrumentopagos_url
  end
end
