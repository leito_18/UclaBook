require 'test_helper'

class StartupmonedasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startupmoneda = startupmonedas(:one)
  end

  test "should get index" do
    get startupmonedas_url
    assert_response :success
  end

  test "should get new" do
    get new_startupmoneda_url
    assert_response :success
  end

  test "should create startupmoneda" do
    assert_difference('Startupmoneda.count') do
      post startupmonedas_url, params: { startupmoneda: { nombre: @startupmoneda.nombre } }
    end

    assert_redirected_to startupmoneda_url(Startupmoneda.last)
  end

  test "should show startupmoneda" do
    get startupmoneda_url(@startupmoneda)
    assert_response :success
  end

  test "should get edit" do
    get edit_startupmoneda_url(@startupmoneda)
    assert_response :success
  end

  test "should update startupmoneda" do
    patch startupmoneda_url(@startupmoneda), params: { startupmoneda: { nombre: @startupmoneda.nombre } }
    assert_redirected_to startupmoneda_url(@startupmoneda)
  end

  test "should destroy startupmoneda" do
    assert_difference('Startupmoneda.count', -1) do
      delete startupmoneda_url(@startupmoneda)
    end

    assert_redirected_to startupmonedas_url
  end
end
