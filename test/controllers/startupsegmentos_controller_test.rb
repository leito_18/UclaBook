require 'test_helper'

class StartupsegmentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startupsegmento = startupsegmentos(:one)
  end

  test "should get index" do
    get startupsegmentos_url
    assert_response :success
  end

  test "should get new" do
    get new_startupsegmento_url
    assert_response :success
  end

  test "should create startupsegmento" do
    assert_difference('Startupsegmento.count') do
      post startupsegmentos_url, params: { startupsegmento: { nombre: @startupsegmento.nombre } }
    end

    assert_redirected_to startupsegmento_url(Startupsegmento.last)
  end

  test "should show startupsegmento" do
    get startupsegmento_url(@startupsegmento)
    assert_response :success
  end

  test "should get edit" do
    get edit_startupsegmento_url(@startupsegmento)
    assert_response :success
  end

  test "should update startupsegmento" do
    patch startupsegmento_url(@startupsegmento), params: { startupsegmento: { nombre: @startupsegmento.nombre } }
    assert_redirected_to startupsegmento_url(@startupsegmento)
  end

  test "should destroy startupsegmento" do
    assert_difference('Startupsegmento.count', -1) do
      delete startupsegmento_url(@startupsegmento)
    end

    assert_redirected_to startupsegmentos_url
  end
end
