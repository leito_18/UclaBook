require 'test_helper'

class EditlandingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @editlanding = editlandings(:one)
  end

  test "should get index" do
    get editlandings_url
    assert_response :success
  end

  test "should get new" do
    get new_editlanding_url
    assert_response :success
  end

  test "should create editlanding" do
    assert_difference('Editlanding.count') do
      post editlandings_url, params: { editlanding: { banner_name: @editlanding.banner_name, banner_uid: @editlanding.banner_uid, bannernewsletter_name: @editlanding.bannernewsletter_name, bannernewsletter_uid: @editlanding.bannernewsletter_uid, contactocorreo: @editlanding.contactocorreo, contactodireccion: @editlanding.contactodireccion, contactonombre: @editlanding.contactonombre, contactotelefono: @editlanding.contactotelefono, contactotextoizq: @editlanding.contactotextoizq, contactotituloizq: @editlanding.contactotituloizq, footerfacebook: @editlanding.footerfacebook, footergoogle: @editlanding.footergoogle, footerinstagram: @editlanding.footerinstagram, footerlinkedin: @editlanding.footerlinkedin, footertwitter: @editlanding.footertwitter, imagenseccion2_name: @editlanding.imagenseccion2_name, imagenseccion2_uid: @editlanding.imagenseccion2_uid, portafolio_texto: @editlanding.portafolio_texto, portafolio_titulo: @editlanding.portafolio_titulo, texto1_seccion2: @editlanding.texto1_seccion2, texto2_seccion2: @editlanding.texto2_seccion2, textobanner: @editlanding.textobanner, textonewsletter: @editlanding.textonewsletter, titulo_seccion2: @editlanding.titulo_seccion2, titulonewsletter: @editlanding.titulonewsletter } }
    end

    assert_redirected_to editlanding_url(Editlanding.last)
  end

  test "should show editlanding" do
    get editlanding_url(@editlanding)
    assert_response :success
  end

  test "should get edit" do
    get edit_editlanding_url(@editlanding)
    assert_response :success
  end

  test "should update editlanding" do
    patch editlanding_url(@editlanding), params: { editlanding: { banner_name: @editlanding.banner_name, banner_uid: @editlanding.banner_uid, bannernewsletter_name: @editlanding.bannernewsletter_name, bannernewsletter_uid: @editlanding.bannernewsletter_uid, contactocorreo: @editlanding.contactocorreo, contactodireccion: @editlanding.contactodireccion, contactonombre: @editlanding.contactonombre, contactotelefono: @editlanding.contactotelefono, contactotextoizq: @editlanding.contactotextoizq, contactotituloizq: @editlanding.contactotituloizq, footerfacebook: @editlanding.footerfacebook, footergoogle: @editlanding.footergoogle, footerinstagram: @editlanding.footerinstagram, footerlinkedin: @editlanding.footerlinkedin, footertwitter: @editlanding.footertwitter, imagenseccion2_name: @editlanding.imagenseccion2_name, imagenseccion2_uid: @editlanding.imagenseccion2_uid, portafolio_texto: @editlanding.portafolio_texto, portafolio_titulo: @editlanding.portafolio_titulo, texto1_seccion2: @editlanding.texto1_seccion2, texto2_seccion2: @editlanding.texto2_seccion2, textobanner: @editlanding.textobanner, textonewsletter: @editlanding.textonewsletter, titulo_seccion2: @editlanding.titulo_seccion2, titulonewsletter: @editlanding.titulonewsletter } }
    assert_redirected_to editlanding_url(@editlanding)
  end

  test "should destroy editlanding" do
    assert_difference('Editlanding.count', -1) do
      delete editlanding_url(@editlanding)
    end

    assert_redirected_to editlandings_url
  end
end
